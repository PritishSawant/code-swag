//
//  Category.swift
//  CodeSwag
//
//  Created by Priitsh Sawant on 02/04/18.
//  Copyright © 2018 Pritish Sawant. All rights reserved.
//

import Foundation

struct Category {
    private(set) public var title:String  //private(set) cannot be set by outside classes
    private(set) public var imageName:String
    
    init(title:String,imageName:String) {
        self.title=title
        self.imageName=imageName
    }
}
