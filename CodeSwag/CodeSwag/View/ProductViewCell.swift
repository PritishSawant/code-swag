//
//  ProductViewCell.swift
//  CodeSwag
//
//  Created by Priitsh Sawant on 02/04/18.
//  Copyright © 2018 Pritish Sawant. All rights reserved.
//

import UIKit

class ProductViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageName: UIImageView!
    @IBOutlet weak var pricelbl: UILabel!
    @IBOutlet weak var labelName: UILabel!
    
   
    
    func updateViews(product:Product){
        imageName.image = UIImage(named: product.imageName)
        pricelbl.text = product.price
        labelName.text = product.title
        
        
    }
}
