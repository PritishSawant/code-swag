//
//  CategoryCell.swift
//  CodeSwag
//
//  Created by Priitsh Sawant on 02/04/18.
//  Copyright © 2018 Pritish Sawant. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
    
    func updateViews(category:Category){
        categoryImage.image = UIImage(named: category.imageName)
        categoryTitle.text = category.title
    }

}
