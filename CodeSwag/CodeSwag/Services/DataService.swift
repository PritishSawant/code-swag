//
//  DataService.swift
//  CodeSwag
//
//  Created by Priitsh Sawant on 02/04/18.
//  Copyright © 2018 Pritish Sawant. All rights reserved.
//

import Foundation

class DataService {
    static let instance = DataService()
    
    private let categories = [
        Category(title: "SHIRTS", imageName: "shirts.png"),
        Category(title: "HOODIES", imageName: "hoodies.png"),
        Category(title: "HATS", imageName: "hats.png"),
        Category(title: "DIGITAL", imageName: "digital.png")
    ]
    
    private let hats = [
        Product(title: "Devslopes Logo Graphic Beanie", imageName: "hat01.png", price: "$18"),
        Product(title: "Devslopes Logo Graphic Beanie2", imageName: "hat02.png", price: "$13"),
        Product(title: "Devslopes Logo Graphic Beanie3", imageName: "hat03.png", price: "$19"),
        Product(title: "Devslopes Logo Graphic Beanie4", imageName: "hat04.png", price: "$12")
    ]

    private let hoodies = [
        Product(title: "Devslopes Logo Graphic Hoodie01", imageName: "hoodie01.png", price: "$18"),
        Product(title: "Devslopes Logo Graphic Hoodie02", imageName: "hoodie01.png", price: "$28"),
        Product(title: "Devslopes Logo Graphic Hoodie03", imageName: "hoodie03.png", price: "$38"),
        Product(title: "Devslopes Logo Graphic Hoodie04", imageName: "hoodie04.png", price: "$8")
    ]
    
    private let shirts = [
        Product(title: "Devslopes Logo Graphic Shirt01", imageName: "shirt01.png", price: "$188"),
        Product(title: "Devslopes Logo Graphic Shirt02", imageName: "shirt02.png", price: "$288"),
        Product(title: "Devslopes Logo Graphic Shirt03", imageName: "shirt03.png", price: "$348"),
        Product(title: "Devslopes Logo Graphic Shirt04", imageName: "shirt04.png", price: "$168"),
        Product(title: "Devslopes Logo Graphic Shirt05", imageName: "shirt05.png", price: "$148")
    ]
    
    private let digitalGoods = [Product]()
    
    func getCategories() -> [Category] {
        return categories
    }
    
    func getProducts(forCategoryTitle title:String) -> [Product]{
        switch title {
        case "HATS":
           return getHats()
        case "HOODIES":
           return getHoodies()
        case "SHIRTS":
           return getShirts()
        case "DIGITALGOODS":
           return getDigitalGoods()
        default:
           return getShirts()
        }
    }
    
    func getHats() -> [Product]{
        return hats
    }
    
    func getHoodies() -> [Product]{
        return hoodies
    }
    
    func getShirts() -> [Product]{
        return shirts
    }
    
    func getDigitalGoods() -> [Product]{
        return digitalGoods
    }
}
