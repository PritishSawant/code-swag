//
//  ProductsVC.swift
//  CodeSwag
//
//  Created by Priitsh Sawant on 03/04/18.
//  Copyright © 2018 Pritish Sawant. All rights reserved.
//

import UIKit

class ProductsVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {
    
    
    @IBOutlet weak var collectionsView: UICollectionView!
    
    private(set) public var products = [Product]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionsView.delegate = self
        collectionsView.dataSource = self
        // Do any additional setup after loading the view.
    }

    func initProducts(category:Category){
        products = DataService.instance.getProducts(forCategoryTitle: category.title)
        navigationItem.title = category.title
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        if let cell = collectionsView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as? ProductViewCell{
            let product = products[indexPath.row]
            cell.updateViews(product: product)
            return cell
        }
        return ProductViewCell()
    }
    
}
